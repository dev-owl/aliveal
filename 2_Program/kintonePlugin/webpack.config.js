const path = require('path');
const KintonePlugin = require('@kintone/webpack-plugin-kintone-plugin');

module.exports = {
    entry: {
        desktop: './src/desktop/index.js',
        mobile: './src/mobile/index.js',
        config: './src/config/index.js'
    },
    output: {
        path: path.resolve(__dirname, 'src' , 'plugin'),
        filename: '[name].js'
    },
    module: {
        loaders: [
            { test: /\.html$/, loader: 'html-loader' },
            { test: /\.css$/, loaders: ['style-loader','css-loader'] },
        ]
    },
    plugins: [
        new KintonePlugin({
            manifestJSONPath: './src/manifest.json',
            privateKeyPath: './private.ppk',
            pluginZipPath: './dist/plugin.zip'
        })
    ]
};